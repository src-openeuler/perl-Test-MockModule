%bcond_with perl_Test_MockModule_enables_optional_test
Name:                perl-Test-MockModule
Version:             0.179.0
Release:             2
Summary:             Override subroutines in a module for unit testing
License:             GPL-1.0-or-later OR Artistic-1.0-Perl
URL:                 https://metacpan.org/release/Test-MockModule
Source0:             https://cpan.metacpan.org/authors/id/G/GF/GFRANKS/Test-MockModule-v%{version}.tar.gz
BuildArch:           noarch
BuildRequires:       coreutils perl-generators perl-interpreter perl(Module::Build) perl(strict)
BuildRequires:       perl(warnings) perl(Carp) perl(Scalar::Util) perl(SUPER) perl(vars) perl(lib)
BuildRequires:       perl(Test::More)	perl(Test::Warnings)
%if %{with perl_Test_MockModule_enables_optional_test}
BuildRequires:       perl(Test::Pod) >= 1.00 perl(Test::Pod::Coverage) >= 1.00
%endif

%description
%{summary}.

%prep
%autosetup  -n Test-MockModule-v%{version} -p1

%build
%{__perl} Build.PL installdirs=vendor
./Build

%install
./Build install destdir=$RPM_BUILD_ROOT create_packlist=0
chmod -R u+w $RPM_BUILD_ROOT/*

%check
./Build test

%files
%doc Changes README.md
%license LICENSE
%{perl_vendorlib}/Test
%{_mandir}/man3/*.3*

%changelog
* Sun Jan 19 2025 Funda Wang <fundawang@yeah.net> - 0.179.0-2
- drop useless perl(:MODULE_COMPAT) requirement

* Wed Sep 25 2024 dfh <dufuhang@kylinos.cn> - 0.179.0-1
- Upgrade to version 0.179.0
- 82881df - Bump Module::Build requirement by @atoomic in #65

* Mon Jun 17 2024 guoshengsheng <guoshengsheng@kylinos.cn> - 0.178.0-1
- Upgrade to version 0.178.0
- 6724a30 - Simplify CI workflow - Nicolas R
- 1801372 - Multiple improvements - Nicolas R
- e97e316 - Add protection to _replace_sub - Nicolas R

* Tue Jun 14 2022 SimpleUpdate Robot <tc@openeuler.org> - 0.177.0-1
- Upgrade to version 0.177.0

* Thu Jul 2 2020 huanghaitao <huanghaitao8@huawei.com> - 0.170.0-2
- package init
